var http = require("http");
const { get } = require("https");
var readline = require('readline-sync');

let data;

http.createServer(async function (request, response) {
    response.writeHead(200, { 'Content-Type': 'text/plain' });

    async function getapi(url) {
        const response = await fetch(url);

        data = await response.json();

        return data;
    }

    const comments = await getapi("https://jsonplaceholder.typicode.com/comments");

    const searchBy = readline.question("Search by what?:\r\n 1. Post Id\r\n 2. Id\r\n 3. Name\r\n 4. Email\r\n 5. Body\r\n");
    const searchWhat = readline.question("Enter what you want to search:\r\n");

    const searchFilter = {
        PostId: 1,
        Id: 2,
        Name: 3,
        Email: 4,
        Body: 5,
    }

    let searchedData;

    var condition = new RegExp(searchWhat);

    if (Number(searchBy) === searchFilter.PostId) {
        searchedData = comments.filter(key => key.postId === Number(searchWhat));

        console.log(searchedData);

    } else if (Number(searchBy) === searchFilter.Id) {
        searchedData = comments.filter(key => key.id === Number(searchWhat));

        console.log(searchedData);
    } else if (Number(searchBy) === searchFilter.Name) {

        searchedData = comments.filter(key => condition.test(key.name));

        console.log(searchedData);
    } else if (Number(searchBy) === searchFilter.Email) {

        searchedData = comments.filter(key => condition.test(key.email));

        console.log(searchedData);
    } else if (Number(searchBy) === searchFilter.Body) {

        searchedData = comments.filter(key => condition.test(key.body));

        console.log(searchedData);
    }

    response.end(JSON.stringify(searchedData));
}).listen(8081);

console.log('Server running at http://127.0.0.1:8081/');