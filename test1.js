var http = require("http");
const { get } = require("https");

let data;

http.createServer(async function (request, response) {
    response.writeHead(200, { 'Content-Type': 'text/plain' });

    async function getapi(url) {
        const response = await fetch(url);

        data = await response.json();

        return data;
    }

    const comments = await getapi("https://jsonplaceholder.typicode.com/comments");

    const posts = await getapi("https://jsonplaceholder.typicode.com/posts")

    const mergedPostComments = posts.map(element => ({ ...element, ...comments.find(key => key.id === element.id) }));

    const groupedPost = {};

    mergedPostComments.forEach(element => {
        const postIdIndex = element.postId;

        if (!groupedPost[postIdIndex]) {
            groupedPost[postIdIndex] = [];
        }

        groupedPost[postIdIndex].push({
            post_title: element.title,
            post_body: element.body,
        });
    });

    const sortedPost = Object.entries(groupedPost).map(([post_id, comments_data]) => ({
        post_id,
        comments_data,
    }))

    const postWithNumberOfComments = sortedPost.map((key) => ({
        ...key,
        total_number_of_comments: key.comments_data.length,
    }))

    response.end(JSON.stringify(postWithNumberOfComments));
}).listen(8081);

console.log('Server running at http://127.0.0.1:8081/');